
# Database

Our software tracks each flight along with its data in an sqlite3 database. This means that current (and previous) flights can be displayed as desired. In order to capture,insert, and display data, All a user has to do is run the 'runall.py' file located in SrDesign/Database. A brief description of runall along with its subcomponents is listed below.

## Dependencies

There are several dependencies required in this project. I have listed them in a script. Running `bash depends.sh` will install them onto your computer. Some items like Datapane will require more effort to get working. Datapane requires a user to create an account and run a command to validate the login. Examples can be found in their documentation: https://docs.datapane.com/tut-getting-started

## Git-Lab

In order for each team member to run tests on their own laptop, we needed a universal database. We ended up simplying adding the .db file into the git repo so each pull could refresh the database. However, GitHub has a 100MB limit. This was quickly exceeded by our database. To overcome this, we began using the GitLab. GitLab allows for the admin of the repo to select the file size. In order to set GitLab up properly, run `git remote set-url origin https://gitlab.com/johnstaa/SrDesign`. When prompted to login, use your username and Access Token (Account->Preferences->Access Token). er


## DatabaseInit.py
This python script creates the `Rocket.db` file which holds all the data for rocket flights. I don't think from a database structure its set up super well, but it suits our purposes. Basically each table ties back to Rocket_Flight with a Flight_ID. This Flight_ID is autoincremented and used to insert, grade, and graph different flights.

## Runall.py

Runall.py is meant to be an overarching control file that attempts to run all files necessary for a test flight. It does this through pythons OS (Check_Call) function. The following headers are the files that Runall.py invokes as well as a brief description of each. If the command line '-eep' is used on this file the data inserted is formatted as eeprom (as opposed to usb)

## InsertData.py

This python script creates a connection with the Rocket.db file. Once connected, it grabs data from SrDesign/Database/Data/Flights and parses through the correct file. It inserts each line into its corresponding table. If anyone has questions specific to the database itself or how it works, please ask Aaron Johnston.

## GradeFlight.py
This python script grades each flight, giving scores to each phase based off roll pitch and yaw. It also scores the altitude based off desired altitude.

## CreateCSV.py

This python script simply creates a CSV file consisting of the data from the specific flight. This is turned into an xlsx file and attached at the bottom of each report. Currently it is turned off as we never really found a use for it and running the file takes a while.

## CreateReport.py

This python script utilizes Pandas, Plotly, and Datapane to query data from the database into dataframes (df). Once the data is in the dataframe, Datapane creates user friendly responsive graphs for viewing. All database querying happens here. 
