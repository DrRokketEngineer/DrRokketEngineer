#   File Name: InsertData.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: September 12th, 2021
#   Purpose: This file is intended to parse and insert data into an sqlite3 database (Rocket.db).
#            
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#

import sqlite3
from sqlite3.dbapi2 import Error
from datetime import datetime
from turtle import right

from numpy import rollaxis
import pynmea2
import pandas as pd
import io
import subprocess
import os
import sys
import time
from math import cos, sqrt, pow, tan, radians, acos, degrees
from subprocess import check_call
import shutil





t1 = time.time()

# Grab the flight ID from CLA
RFid = -1
# EEPROM or USB
flightType = 'None'
# Short description
flightDesc = 'None'
eeprom=False
if (len(sys.argv) > 1):
    RFid = sys.argv[1]
if (len(sys.argv) > 2):
        eeprom=sys.argv[2]
if (len(sys.argv) > 3):
        flightDesc = sys.argv[3]
if (len(sys.argv) > 4):
        flightLocation = sys.argv[4]

# File path to pull data from
dataFile = f'../Data/Flights/Flight{RFid}.txt'

# Open db and create a cursor
con = sqlite3.connect('../Rocket.db')
db = con.cursor()
db.execute('PRAGMA foreign_keys = ON') # This is required as default sqlite turns off FK's


#########################################################
# Backup for Database
# This calls the iterdump function on the sqlite3 python function
# Creates a .sql file 
# If dump is passed as true, the Rocket.db file is removed, and then recreated from the .sql file
# Due to this, the user must be 100% confident that the .sql file contains everything necessary to recreate the .db file
# NOT REALLY USED ANYMORE
#########################################################
def backupDB(dump=False):
    # Dump writes the sql file into the db file
    if(dump):
        subprocess.call(['touch', 'Rocket.db'])
        os.system('cat backupRocket.sql | sqlite3 Rocket.db')
        print('Successfully Backed up database . . .')
    else:
        with io.open('backupRocket.sql', 'w') as p:
            for line in con.iterdump():
                p.write('%s\n' % line)


#########################################################
# Helper function to calculate gamma (Degree of center)
#########################################################
def calculateGamma(r, p):
    r = float(r)
    p = float(p)
    return(degrees(acos(cos(radians(r))/(sqrt(1+pow(tan(radians(p)),2)*pow(cos(radians(r)),2)))))
)



#####################################################################################
# Parse and Insert Data File from Database/Data/Flights with USB formatting
#####################################################################################
def insertAllDataUSB():
    # Enter a new Rocket Flight entry
    flightType = 'USB'
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    db.execute(''' INSERT INTO Rocket_Flight(Date, Type, Description,Location) 
                                                    VALUES (?,?,?,?)''',
                                                    (dt_string,flightType,flightDesc,flightLocation) )    
    # Need to commit this now so its ready for the rest of the inserts.
    con.commit()

    file = open(dataFile ,'r')
    # Output GPS data to a separate file and let the helper function parse and insert 
    outfile = open('../Data/Flights/gpsDataTest.txt', 'w')
    phases =  [-1]*6
    # I know this is a super non-pythonic way to do it but it helps with inserts
    i = 0
    j = 0
    z = 0
    servoFiveTest = []
    for x in file:
        if (x != '\n'):
            # Write GPS Data
            if('GGA' in x):
                outfile.write(x + '\n')
            # Insert IMU Data
            if ('GYR' in x):
                imuData = x.split(' ')
                gamma = calculateGamma(imuData[16],imuData[18])
                db.execute('''INSERT INTO IMU_Data(Flight_ID, Time, GyroX, GyroY, GyroZ, AccX, AccY, AccZ, Roll, Pitch, Yaw, gamma)
                            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)''',
                            (RFid, i ,imuData[2],imuData[4],imuData[6],imuData[9], imuData[11],imuData[13],imuData[16], imuData[18],imuData[20].strip(), gamma))
                i += 1
            # Insert Roll Pitch and Yaw Values
            elif('RPY' in x):
                x = x.split(' ')
                db.execute('''INSERT INTO IMU_Data(Flight_ID, Time, Roll, Pitch, Yaw)
                            VALUES(?,?,?,?,?)''',
                            (RFid, i ,x[2],x[4],x[6].strip()))
                i +=1
            # Insert Laser Range Data
            elif('RAN' in x):
                if ('out of range' not in x):
                    x = x.split(' ')
                    alt=float(x[1].strip())
                    speed = float(x[3].strip())
                    acc = float(x[5].strip())
                    AltPID = float(x[7].strip())
                    desAlt = float(x[9].strip())
                    db.execute(''' INSERT INTO Laser_Data(Flight_ID, Time, Laser_Float,Speed,Acceleration,AltPID,desAlt) 
                                                            VALUES (?,?,?,?,?,?,?)''',
                                                            (RFid , i, alt, speed,acc,AltPID,desAlt))
            # Insert Servo Data
            elif('SER' in x):
                x = x.split(' ')
                servoFiveTest.append(float(x[10]))
                db.execute(''' 
                            INSERT INTO Servo(Flight_ID, Time, Servo_One, Servo_Two, Servo_Three, 
                            Servo_Four, Servo_Five, Servo_Six, Servo_Seven, Servo_Eight)
                            VALUES(?,?,?,?,?,?,?,?,?,?)''',
                            (RFid,j,x[2],x[4],x[6],x[8],x[10],x[12],x[14].strip(), 0))
                j+=1
            # Insert Gain Values
            elif('GAIN' in x):
                x = x.split(' ')
                pitchP = abs(float(x[2]))
                pitchI= abs(float(x[4]))
                pitchD= abs(float(x[6]))
                rollP= abs(float(x[8]))
                rollI= abs(float(x[10]))
                rollD= abs(float(x[12]))
                pitchSum = pitchP+pitchI+pitchD
                rollSum = rollP+rollI+rollD
                # Old way of doing things.. left for backwards compatability
                if len(x) < 14:
                    db.execute('''INSERT INTO Gains(Flight_ID,Time,KpPitch,KiPitch,KdPitch,KpRoll,KiRoll,KdRoll,kpAlt,kiAlt,kdAlt)
                               VALUES(?,?,?,?,?,?,?,?,?,?,?)''',
                                     (RFid,z,pitchP/pitchSum,pitchI/pitchSum,pitchD/pitchSum,
                                     rollP/rollSum,rollI/rollSum,rollD/rollSum,
                                     0,0,0))
                # Current way of doing things
                # This basically inserts the percentages that each gain (P, I, or D) affected the rocket
                else:
                    altP= abs(float(x[14]))
                    altI= abs(float(x[16]))
                    altD= abs(float(x[18]))
                    altSum =  altP+altI+altD
                    if (pitchSum != 0 and rollSum != 0 and altSum != 0):
                        db.execute('''INSERT INTO Gains(Flight_ID,Time,KpPitch,KiPitch,KdPitch,KpRoll,KiRoll,KdRoll,kpAlt,kiAlt,kdAlt)
                                        VALUES(?,?,?,?,?,?,?,?,?,?,?)''',
                                            (RFid,z,round(pitchP/pitchSum,2),round(pitchI/pitchSum,2),round(pitchD/pitchSum,2),
                                            round(rollP/rollSum,2),round(rollI/rollSum,2),round(rollD/rollSum,2),
                                            round(altP/altSum,2),round(altI/altSum,2),round(altD/altSum,2)))
                    # We only control alt in landing so it will be 0 everywhere else. Therefore we need to check that here
                    elif (pitchSum != 0 and rollSum != 0):
                        db.execute('''INSERT INTO Gains(Flight_ID,Time,KpPitch,KiPitch,KdPitch,KpRoll,KiRoll,KdRoll,kpAlt,kiAlt,kdAlt)
                                    VALUES(?,?,?,?,?,?,?,?,?,?,?)''',
                                        (RFid,z,round(pitchP/pitchSum,2),round(pitchI/pitchSum,2),round(pitchD/pitchSum,2),
                                        round(rollP/rollSum,2),round(rollI/rollSum,2),round(rollD/rollSum,2),
                                        0,0,0))
                z+=1
            # Old way of selecting phase. Kept for backwards compatability with flights
            elif('In ' in x):
                if ('PreTakeoff') in x:
                    phases[0] = i
                elif ('Takeoff') in x:
                    phases[1] = i
                elif ('FlightPeak') in x:
                    phases[2] = i
                elif ('MajorDescent') in x:
                    phases[3] = i
                elif ('PostLanding') in x:
                    phases[5] = i
                elif ('Landing') in x:
                    phases[4] = i
            # New way of selecting phase
            elif ('Flight Sequence Phase' in x):
                phase = x.split(': ')
                phase = phase[1].strip()
                if (phase=='0'):
                    phases[0] = i
                elif (phase=='1'):
                    phases[1] = i
                elif (phase=='2'):
                    phases[2] = i
                elif (phase=='3'):
                    phases[3] = i
                elif (phase=='4'):
                    phases[4] = i
                elif (phase=='5'):
                    phases[5] = i
            else:
                print(f'junk line {x}')
                continue
    # Add in phases right at the end if they dont already exist... helps with errors
    for i in range(len(phases)):
        if phases[i] == -1:
            phases[i] = max(phases) + 1

    db.execute('''  UPDATE Rocket_Flight SET
            PhaseZeroTime = ?,
            PhaseOneTime = ?,
            PhaseTwoTime = ?,
            PhaseThreeTime = ?,
            PhaseFourTime = ?,
            PhaseFiveTime = ?
            WHERE Flight_ID = ?
            ''',
            (phases[0],phases[1],phases[2],phases[3],phases[4],phases[5],RFid))

#########################################################
# Parse and insert GPS - Not currently in use
#########################################################
def parseInsertGPS():
    file = open('../Data/EOL/gpsData.txt', 'r') # This is just static data so that we're visualizing some GPS with reports
    i = 0
    first = True
    firstLat = 0
    firstLon = 0
    firstAlt = 0
    for line in file.readlines():
        try:
            msg = pynmea2.parse(line)
            if ('GGA' in line):
                if (msg.lat != None and msg.lon != None and msg.altitude != None):
                    if (first):
                        firstLat = float(msg.lat)
                        firstLon = float(msg.lon)
                        firstAlt = float(msg.altitude)
                        first = False
                    else:
                        # Baseline data off the first data point
                        lat = float(msg.lat) - firstLat
                        lon = float(msg.lon) - firstLon
                        alt = float(msg.altitude) - firstAlt
                        db.execute(''' INSERT INTO GPS_Data(Flight_ID, Time, PositionX, PositionY, PositionZ) 
                                        VALUES (?,?,?,?,?)''',
                                        (RFid , i, lat, lon, alt))
                        i += 1
        except pynmea2.ParseError as e:
            continue
    

#####################################################################################
# Parse and Insert Data File from Database/Data/Flights with EEPROM formatting
#####################################################################################
def insertAllDataEEPROM():
    # Enter a new Rocket Flight entry
    errorCount=0
    flightType = 'EEPROM'
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    db.execute(''' INSERT INTO Rocket_Flight(Date, Type, Description,Location) 
                                                    VALUES (?,?,?,?)''',
                                                    (dt_string,flightType,flightDesc,flightLocation) )    
    # Need to commit this now so its ready for the rest of the inserts.
    con.commit()

    file = open(dataFile ,'r')
    # Output GPS data to a separate file and let the helper function parse and insert 
    phases =  [-1]*6
    i = 0
    t = 0.0 # Variable used for time
    for x in file:
        # print(x)
        t += 0.01
        t = round(t,2)
        if (x != '\n'):
            data = x.split(',')
            # Junk Line
            if(len(data)!=25):
                errorCount+=1
                print(f'Data loss at {t}')
                continue
            #Strip off extra character from EEPROM
            gyroX=data[0].lstrip("B")
            gyroY=data[1]
            gyroZ=data[2]
            accX=data[3]
            accY=data[4]
            accZ=data[5]
            roll=data[6]
            pitch=data[7]
            yaw=data[8]
            s7=data[9]
            pitchP = abs(float(data[10]))
            pitchI= abs(float(data[11]))
            pitchD= abs(float(data[12]))
            rollP= abs(float(data[13]))
            rollI= abs(float(data[14]))
            rollD= abs(float(data[15]))
            altP= abs(float(data[16]))
            altI= abs(float(data[17]))
            altD= abs(float(data[18]))
            altitude=data[19]
            phase=data[20]
            speed = data[21]
            acc = data[22]
            AltPID = data[23]
            desAlt = float(data[24])
            # Insert IMU Data
            gamma = calculateGamma(roll,pitch)
            db.execute('''INSERT INTO IMU_Data(Flight_ID, Time, GyroX, GyroY, GyroZ, AccX, AccY, AccZ, Roll, Pitch, Yaw, gamma)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?)''',
                        (RFid, t ,gyroX,gyroY,gyroZ,accX,accY,accZ,roll,pitch,yaw, gamma))           
            # Insert Laser Range Data
            db.execute(''' INSERT INTO Laser_Data(Flight_ID, Time, Laser_Float,Speed,Acceleration,AltPID,Desired_Altitude) 
                                                        VALUES (?,?,?,?,?,?,?)''',
                                                        (RFid , t, altitude, speed,acc,AltPID,desAlt))
            # Insert Servo Data
            db.execute(''' 
                        INSERT INTO Servo(Flight_ID, Time, Servo_One, Servo_Two, Servo_Three, 
                        Servo_Four, Servo_Five, Servo_Six, Servo_Seven)
                        VALUES(?,?,?,?,?,?,?,?,?)''',
                        (RFid,t,0,0,0,0,0,0,s7))
            # Insert Gain Values
            pitchSum = pitchP+pitchI+pitchD
            rollSum = rollP+rollI+rollD
            altSum =  altP+altI+altD

            if (pitchSum != 0 and rollSum != 0 and altSum != 0):
                db.execute('''INSERT INTO Gains(Flight_ID,Time,KpPitch,KiPitch,KdPitch,KpRoll,KiRoll,KdRoll,kpAlt,kiAlt,kdAlt)
                                VALUES(?,?,?,?,?,?,?,?,?,?,?)''',
                                    (RFid,t,round(pitchP/pitchSum,2),round(pitchI/pitchSum,2),round(pitchD/pitchSum,2),
                                    round(rollP/rollSum,2),round(rollI/rollSum,2),round(rollD/rollSum,2),
                                    round(altP/altSum,2),round(altI/altSum,2),round(altD/altSum,2)))
            # We only control alt in landing so it will be 0 everywhere else. Therefore we need to check that here
            elif (pitchSum != 0 and rollSum != 0):
                db.execute('''INSERT INTO Gains(Flight_ID,Time,KpPitch,KiPitch,KdPitch,KpRoll,KiRoll,KdRoll,kpAlt,kiAlt,kdAlt)
                            VALUES(?,?,?,?,?,?,?,?,?,?,?)''',
                                (RFid,t,round(pitchP/pitchSum,2),round(pitchI/pitchSum,2),round(pitchD/pitchSum,2),
                                round(rollP/rollSum,2),round(rollI/rollSum,2),round(rollD/rollSum,2),
                                0,0,0))
            # Mark down each phase
            if (phase=='0'):
                phases[0] = t
            elif (phase=='1'):
                phases[1] = t
            elif (phase=='2'):
                phases[2] = t
            elif (phase=='3'):
                phases[3] = t
            elif (phase=='4'):
                phases[4] = t
            elif (phase=='5'):
                phases[5] = t
            i += 1
    print('Total Errors: ',errorCount)
    # Add in phases right at the end if they dont already exist... helps with errors
    for i in range(len(phases)):
        if phases[i] == -1:
            phases[i] = max(phases) + 1

    db.execute('''  UPDATE Rocket_Flight SET
            PhaseZeroTime = ?,
            PhaseOneTime = ?,
            PhaseTwoTime = ?,
            PhaseThreeTime = ?,
            PhaseFourTime = ?,
            PhaseFiveTime = ?
            WHERE Flight_ID = ?
            ''',
            (phases[0],phases[1],phases[2],phases[3],phases[4],phases[5],RFid))
    

#####################################################################################
# Check the amount of data errors from serial transfer.
# If there are more than 5 errors, dont try and insert data
#####################################################################################
def checkSerialTransfer():
    file = open(dataFile ,'r')
    errorCount = 0
    for x in file:
        if (x != '\n'):
            data = x.split(',')
            # Junk Line
            if(len(data)!=25):
                errorCount+=1
    print(errorCount)
    return errorCount

        

if(checkSerialTransfer() > 20):
    print('Bad Serial Transfer, re-attempting to collect data')
    os.chdir('../../SerialTestPython')
    check_call(['python3','collectData.py', f'{RFid}','True',f'{eeprom}']) 
    print('Finished Collecting Data . . .')
    # Move Data to correct Location
    print('Attempting to move data file to directory . . .') 
    # Remove flight data file if text already exsists
    subprocess.call(['rm', f'../Database/Data/Flights/Flight{RFid}.txt']) 
    shutil.move(f'Flight{RFid}.txt', '../Database/Data/Flights')
    os.chdir('../Database/Insertion')
    check_call(['python3','insertData.py', f'{RFid}',f'{eeprom}', f'{flightDesc}',f'{flightLocation}'])
    sys.exit(0)
else:
    if (eeprom == 'True'):
        insertAllDataEEPROM()
    else:
        insertAllDataUSB()
    parseInsertGPS()
    print(f'All data Inserted . . . Flight ID: {RFid}')


#commit the changes to db           
con.commit()
#close the connection
con.close()

print(f"Time to Insert: {round(time.time()-t1,3)}")