#   File Name: GradeFlight.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: December 6th, 2021
#   Purpose: This file's purpose is to take a given flights data, and assign it a "grade"
#           The drive behind this is to be able to quickly see if adjustments were beneficial or not
#           In short, a grade is given based off the time it takes for the rocket to readjust itself from a disturbance.
#           The base equation is as follows: (TR-TH)/MA 
#           Where:  TR = Time till Recovery
#                   TH = Time Hit
#                   MA = Max Angle (Amount disturbed)
#            
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#


import sqlite3
from numpy.core.fromnumeric import size
from numpy.lib.function_base import average
import pandas as pd
import numpy as np
import sys
from math import cos, sqrt, pow, tan, radians, acos, degrees
import time

t1 = time.time()

# Calculate a rolling average with window size (n) for a given list (a)
def rolling_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


# Grab the flight ID from CLA
RFid = -1
if (len(sys.argv) > 1):
    RFid = sys.argv[1]

# Open db and create a cursor
con = sqlite3.connect('../Rocket.db')
db = con.cursor()
db.execute('PRAGMA foreign_keys = ON') # This is required as default sqlite turns off FK's


def gradeRPYPhase(phase='None'):
    if (phase == 'PreTakeOff'):
        start = 0
        stop = pd.read_sql(('Select PhaseZeroTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    elif(phase == 'Takeoff'):
        start = pd.read_sql(('Select PhaseZeroTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
        stop = pd.read_sql(('Select PhaseOneTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    elif(phase == 'FlightPeak'):
        start = pd.read_sql(('Select PhaseOneTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
        stop = pd.read_sql(('Select PhaseTwoTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    elif(phase == 'MajorDescent'):
        start = pd.read_sql(('Select PhaseTwoTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
        stop = pd.read_sql(('Select PhaseThreeTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    elif(phase == 'Landing'):
        start = pd.read_sql(('Select PhaseThreeTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
        stop = pd.read_sql(('Select PhaseFourTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    elif(phase == 'PostLanding'):
        start = pd.read_sql(('Select PhaseFourTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
        stop = pd.read_sql(('Select PhaseFiveTime FROM Rocket_Flight WHERE Flight_ID = ? '),con,params=[RFid])
    
    if (not isinstance(start,int)):
        start = np.array(start.values).reshape(1,start.size).tolist()[0]
        start = start[0]
    stop = np.array(stop.values).reshape(1,stop.size).tolist()[0]
    stop = stop[0]
    # Query Data
    roll = pd.read_sql(('Select Roll FROM IMU_Data WHERE Flight_ID = ? AND Time BETWEEN ? and ? '),con,params=[RFid, start, stop])
    pitch = pd.read_sql(('Select Pitch FROM IMU_Data WHERE Flight_ID = ? AND Time BETWEEN ? and ? '),con,params=[RFid, start, stop])
    # Reshape data into a 1D array
    roll = np.array(roll.values).reshape(1,roll.size).tolist()[0]
    pitch = np.array(pitch.values).reshape(1,pitch.size).tolist()[0]
    # Sum Squares of Arrays
    rollScore = sum(pow(number,2) for number in roll)
    pitchScore = sum(pow(number,2) for number in pitch)
    # Average the scores and scale by time
    score = (rollScore + pitchScore)/2
    time = (stop-start)
    if (time != 0 ):
        score = score/time
    # Round score off
    score = round(score,3)
    # Update the Correct table
    if (phase == 'PreTakeOff'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseZeroScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    elif(phase == 'Takeoff'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseOneScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    elif(phase == 'FlightPeak'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseTwoScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    elif(phase == 'MajorDescent'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseThreeScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    elif(phase == 'Landing'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseFourScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    elif(phase == 'PostLanding'):
        db.execute(''' UPDATE Rocket_Flight SET
                        PhaseFiveScore = ?
                        WHERE Flight_ID = ?
                        ''',
                        (score,RFid) )  
    
def gradeLaser():
    alt = pd.read_sql(('Select Laser_Float FROM Laser_Data WHERE Flight_ID = ?'),con,params=[RFid])
    desAlt = pd.read_sql(('Select Desired_Altitude FROM Laser_Data WHERE Flight_ID = ?'),con,params=[RFid])
    maxSample = pd.read_sql(('''SElECT MAX (Time) from IMU_Data where Flight_ID = ?'''),con,params=[RFid])
    # Reshape data into a 1D array
    alt = np.array(alt.values).reshape(1,alt.size).tolist()[0]
    desAlt = np.array(desAlt.values).reshape(1,desAlt.size).tolist()[0]
    maxSample = maxSample.values.tolist()[0]
    # Sum score and scale by time
    score = 0
    for x,y in zip(alt, desAlt):
        score += abs(float(x)-float(y))
    if (maxSample[0] != 0):
        score = score/maxSample[0]
        
    db.execute(''' UPDATE Laser_Data SET
                            Score = ?
                            WHERE Flight_ID = ?
                            ''',
                            (score,RFid) )  
        

gradeRPYPhase('PreTakeOff')
gradeRPYPhase('Takeoff')
gradeRPYPhase('FlightPeak')
gradeRPYPhase('MajorDescent')
gradeRPYPhase('Landing')
gradeRPYPhase('PostLanding')
gradeLaser()


con.commit()

print(f'time to grade: {round(time.time()-t1,3)} seconds')