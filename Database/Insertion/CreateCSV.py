#   File Name: CreateCSV.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: October 29th, 2021
#   Purpose: This file creates a CSV file that is eventually linked to the reporting site. 
#            The data consists of multiple sheets, each having their own sensors data.
#            
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#

import sqlite3
import pandas as pd
import sys
import time

t1 = time.time()

rf1 = 0
if (len(sys.argv) > 1):
  rf1 = sys.argv[1]

# Open a connection to the Database
con = sqlite3.connect('../Rocket.db')
db = con.cursor()
# Create a pandas excel writer object
writer = pd.ExcelWriter(f'../Data/CSV/Flight{rf1}.xlsx', engine='xlsxwriter')


# Helper function to run queries
def run_query(q):
    with sqlite3.connect('../Rocket.db') as conn:
        return pd.read_sql(q,conn)

# Grab All Data

#
# This is Currently commented out because it takes quite a long time.. I dont think the query is correct

# q = f''' select *
#         from Rocket_Flight RF
#         JOIN IMU_DATA IMU
#         ON RF.Flight_ID = IMU.Flight_ID
#         LEFT OUTER JOIN Laser_Data Laser
#         ON RF.Flight_ID = Laser.Flight_ID
#         AND RF.Flight_ID = 
#         {rf1}
#     '''
# df = run_query(q)
# Collect all GPS Data
# df.to_excel(writer, sheet_name='All Data')
q = f''' select *
        from GPS_Data
        where GPS_Data.Flight_ID = {rf1}
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='GPS')
# Collect all IMU Data
q = f''' select *
        from IMU_Data
        where IMU_Data.Flight_ID = {rf1}
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='IMU')
# Collect all Laser Data
q = f''' select *
        from Laser_Data
        where Laser_Data.Flight_ID = {rf1}
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='Laser')
# Collect all Fan Data
q = f''' select Servo_Seven
        from Servo
        where Servo.Flight_ID = {rf1} 
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='Fan')
# Collect all Servo Data
q = f''' select *
        from Servo
        where Servo.Flight_ID = {rf1}
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='Servo')
# Collect all Gain Data
q = f''' select *
        from Gain
        where Gain.Flight_ID = {rf1}
    '''
df = run_query(q)
df.to_excel(writer,sheet_name='Gain')

writer.save()

print(f'Time to make CSV: {round(time.time()-t1,3)}')