#   File Name: DatabaseInit.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: September 1st, 2021
#   Purpose: This file creates all relationships, keys, and tables necessary for tracking rocket data
#            
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#
import sqlite3


con = sqlite3.connect('Rocket.db')
db = con.cursor()

# # Create Rocket Flight table
# db.execute('''CREATE TABLE Rocket_Flight (
#                 Flight_ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Date text, 
#                 Type text,
#                 rollScore,
#                 pitchScore,
#                 yawScore,
#                 PhaseZero,
#                 PhaseOne,
#                 PhaseTwo,
#                 PhaseThree,
#                 PhaseFour,
#                 PhaseFive,
#                 gammaScore,
#                 PhaseZeroScore,
#                 PhaseOneScore,
#                 PhaseTwoScore,
#                 PhaseThreeScore,
#                 PhaseFourScore,
#                 PhaseFiveScore,
#                 Description,
#                 Location,
#                 )''')
# ''' Type examples
# Date: DD-MM-YY
# Type: 'Vertical Test Stand'
# '''

# # IMU Data Table
# db.execute('''CREATE TABLE IMU_Data (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 GyroX real, 
#                 GyroY real,
#                 GyroZ real, 
#                 AccX real,
#                 AccY real,
#                 AccZ real,
#                 Roll real,
#                 Pitch real,
#                 Yaw real,
#                 gamma,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')
# ''' Type Examples
# GyroX: Roll angular rate (degrees/sec)
# GyroY: Pitch angular rate (degrees/sec)
# GyroZ: Yaw angular rate (degrees/sec)
# AccX:  X-axis accelerations (G's)
# AccY:  Y-axis accelerations (G's)
# AccZ:  Z-axis accelerations (G's)
# Roll:  Roll angle (degrees)
# Pitch: Pitch angle (degrees)
# Yaw:   Yaw angle (degrees)
# '''

# # GPS Data Table
# db.execute('''CREATE TABLE GPS_Data (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 PositionX real,
#                 PositionY real,
#                 PositionZ real,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')

# ''' Type Examples
# Flight_ID: Foreign key relating to Rocket Flight
# Time: Each entry has a time associated with it
# Position X: X-axis GPS coordinate 
# Position Y: Y-axis GPS coordinate 
# Position Z: Z-axis GPS coordinate 
# '''

# # Servo Data Table
# db.execute('''CREATE TABLE Servo (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 Servo_One,
#                 Servo_Two,
#                 Servo_Three,
#                 Servo_Four,
#                 Servo_Five,
#                 Servo_Six,
#                 Servo_Seven,
#                 Servo_Eight,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')

# ''' Type Examples
# Flight_ID: Foreign key relating to Rocket Flight
# Time: Each entry has a time associated with it
# Servo1-6: Float of each servo position
# Servo 7: Fan Speed
# '''

# # Laser Data Table
# db.execute('''CREATE TABLE Laser_Data (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 Laser_Float,
#                 Speed,
#                 Acceleration,
#                 AltPID,
#                 Desired_Altitude,
#                 Score,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')

# ''' Type Examples
# Flight_ID: Foreign key relating to Rocket Flight
# Time: Each entry has a time associated with it
# Laser_Float: Float of Laser data
# '''

# # Gain Data Table
# db.execute('''CREATE TABLE Gain (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 KpPitch,
#                 KiPitch,
#                 KdPitch,
#                 KpRoll,
#                 KiRoll,
#                 KdRoll,
#                 KpYaw,
#                 KiYaw,
#                 KdYaw,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')

# ''' Type Examples
# Flight_ID: Foreign key relating to Rocket Flight
# Time: Each entry has a time associated with it
# These are all gain constants for PID in each axes
# '''




# db.execute(''' CREATE TABLE Rocket_Flight_Backup (
#                 Flight_ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Date text, 
#                 Type text,
#                 Description,
#                 Location,
#                 gammaScore,
#                 PhaseZeroTime,
#                 PhaseOneTime,
#                 PhaseTwoTime,
#                 PhaseThreeTime,
#                 PhaseFourTime,
#                 PhaseFiveTime,
#                 PhaseZeroScore,
#                 PhaseOneScore,
#                 PhaseTwoScore,
#                 PhaseThreeScore,
#                 PhaseFourScore,
#                 PhaseFiveScore
#                 )''')


# db.execute(''' INSERT INTO Rocket_Flight_Backup (Flight_ID, Date, 
#                                     Type,
#                                     Description,
#                                     Location,
#                                     gammaScore,
#                                     PhaseZeroTime,
#                                     PhaseOneTime,
#                                     PhaseTwoTime,
#                                     PhaseThreeTime,
#                                     PhaseFourTime,
#                                     PhaseFiveTime,
#                                     PhaseZeroScore,
                                    # PhaseOneScore,
                                    # PhaseTwoScore,
                                    # PhaseThreeScore,
                                    # PhaseFourScore,
                                    # PhaseFiveScore)
# SELECT 
#     Flight_ID,
#     Date, 
#     Type,
#     Description,
#     Location,
#     gammaScore,
#     PhaseZero,
#     PhaseOne,
#     PhaseTwo,
#     PhaseThree,
#     PhaseFour,
#     PhaseFive,
#     PhaseZeroScore,
#     PhaseOneScore,
#     PhaseTwoScore,
#     PhaseThreeScore,
#     PhaseFourScore,
#     PhaseFiveScore
# FROM Rocket_Flight
#             ''')


# db.execute(''' DROP TABLE Rocket_Flight''')
# db.execute(''' Alter Table Rocket_Flight_Backup RENAME to Rocket_Flight''')

# ## NOW FOR GAINS

# db.execute('''CREATE TABLE Laser_DataBACKUP (
#                 ID INTEGER PRIMARY KEY AUTOINCREMENT,
#                 Flight_ID INTEGER,
#                 Time real, 
#                 Laser_Float real,
#                 Speed real,
#                 Acceleration real,
#                 AltPID real,
#                 Desired_Altitude real,
#                 Score real,
#                 FOREIGN KEY (Flight_ID) REFERENCES Rocket_Flight(Flight_ID)
#                 ON DELETE CASCADE
#                 )''')

# db.execute(''' INSERT INTO Laser_DataBACKUP(Flight_ID,Time,Laser_Float,Speed,Acceleration,AltPID,Desired_Altitude,
#                                             Score) SELECT Flight_ID,Time,Laser_Float,Speed,Acceleration,AltPID,Desired_Altitude,Score
#                                                     FROM Laser_Data
#                                             ''')
# db.execute(''' DROP TABLE Laser_Data''')
# db.execute(''' Alter TABLE Laser_DataBACKUP RENAME to Laser_Data''')
    

# # db.execute(''' INSERT INTO Gain_BACKUP (Flight_ID,
#                                         Time, 
#                                         KpPitch,
#                                         KiPitch,
#                                         KdPitch,
#                                         KpRoll,
#                                         KiRoll,
#                                         KdRoll,
#                                         KpAlt,
#                                         KiAlt,
#                                         KdAlt)
#                                         SELECT Flight_ID,
#                                         Time,
#                                         KpPitch,KiPitch,
#                                         KdPitch,
#                                         KpRoll,
#                                         KiRoll,
#                                         KdRoll,
#                                         KpYaw,
#                                         KiYaw,
#                                         KdYaw
#                                          FROM Gains''')

# db.execute(''' Drop Table Gains''')
# db.execute('''Alter Table Gain_BACKUP RENAME to Gain ''')
# #Gains are good

# db.execute('''DELETE FROM Rocket_Flight where Flight_ID < 500''')
# db.execute('''DELETE FROM IMU_Data where Flight_ID < 500''')
# db.execute('''DELETE FROM Servo where Flight_ID < 500''')
# db.execute('''DELETE FROM Gains where Flight_ID < 500''')
# db.execute('''DELETE FROM Laser_Data where Flight_ID < 500''')
# db.execute('''DELETE FROM GPS_Data where Flight_ID < 500''')
# db.execute('''VACUUM''')

# Save (commit) the changes
con.commit()
con.close()
