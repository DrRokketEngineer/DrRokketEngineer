#   File Name: CreateReport.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: November 28th, 2021
#   Purpose: This file utilizes Datapane, an open source library that allows a simplified way of making a reporting site.
#            Plotly and Pandas are also used to help query and graph the data before it is handed off to Datapane.
#            
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#

##############################################################################################################################################
# NOTE: I am aware of the following error regarding the way the database is interacted with in this file.
#       1. Statements arent queryed using joins. They are each individually queryed. Anyone who knows anything 
#           about databases will probably puke when they see this. However, it is an issue I was unable to ammend
#           given my time restraints. When making interactive plots, the plotly library requires the data to be in a very
#           specific format. I was unable to appease the format required using joins. So I just queryed everything individually
##############################################################################################################################################

import sqlite3
from sqlite3.dbapi2 import Error
import pandas as pd
import sys
import math
import datapane as dp
import plotly.express as px
import time

import plotly.graph_objects as go


t1 = time.time()


# If at least one of these is not set.. program hits a sys.exit(1)
rf1 = False
rf2 = False
if (len(sys.argv) > 1):
  rf1 = sys.argv[1]
  if (len(sys.argv) > 2):
    rf2 = sys.argv[2]
else:
  print('* * * One flight ID must be provided to graph * * *')
  print('* * * Exception Caused in createReport.py due to the preceding error * * *')
  sys.exit(1)

# Path to Database
db = '../Rocket.db' 
# Where to output the .html file
saveFile = f'./Reports/report{rf1}.html'
# Create a connection to database
conn = sqlite3.connect(db)

# List time of each pphases
phases = pd.read_sql((''' SELECT PhaseZeroTime, PhaseOneTime, PhaseTwoTime,
                              PhaseThreeTime, PhaseFourTime, PhaseFiveTime
                              FROM Rocket_Flight
                              WHERE Flight_ID = ?
                      '''),conn,params=[rf1])
phases = (phases.values.tolist()[0])
# Max Sample in Flight
maxSample = pd.read_sql(('''SElECT MAX (Time) from IMU_Data where Flight_ID = ?'''),conn,params=[rf1])
maxSample = maxSample.values.tolist()[0]
# Time of rocket Takeoff
startTakeoff = pd.read_sql(('''SELECT MIN(Time) from Laser_Data where Flight_ID = ? and Laser_Float >= 7'''),conn,params=[rf1])
startTakeoff = startTakeoff.values.tolist()[0]
# Description of Rocket Flight
flightDesc = pd.read_sql(('''Select Description from Rocket_Flight where Flight_ID = ? '''),conn,params=[rf1])
flightDesc = flightDesc.values.tolist()[0]
# Highest Laser Value
flightPeak = pd.read_sql(('''Select max(Laser_Float) from Laser_Data where Flight_ID = ? AND Time <= ? '''),conn,params=[rf1,phases[3]])
flightPeak = flightPeak.values.tolist()[0]
flightPeak = float(flightPeak[0])
print(f'FlightPeak : {flightPeak}')
# Select Time of Flight peak
flightPeakTime = pd.read_sql((''' Select Time from laser_Data where Flight_ID = ? AND Laser_Float = ?'''),conn,params=[rf1,flightPeak])
flightPeakTime = flightPeakTime.values.tolist()[0]
print(f'at time: {flightPeakTime}')

# Super unnecessary HTML/CSS Stuff
header = """
<html>
    <style type='text/css'>
        @keyframes example {
            0%   {color: #EEE;}
            25%  {color: #EC4899;}
            50%  {color: #8B5CF6;}
            100% {color: #EF4444;}
        } 
        h1 {
            color:#eee;
            animation-name: example;
            animation-duration: 4s;
            animation-iteration-count: infinite;
        }
    </style>
    <div id="container">
    """
header += f'<h1> &#128640;&#128640;&#128640; Rocket Flight {rf1} &#128640;&#128640;&#128640; </h1> </div> </html>'
header += f'<p> {flightDesc[0]} </p>'
##################################################################################################################
# This helper function creates a query for Gyro Data
##################################################################################################################
def query_gyro():
  qX = pd.read_sql(('Select Time as Time, GyroX as Gyro, \'X\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  qY = pd.read_sql(('Select Time as Time, GyroY as Gyro, \'Y\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  qZ = pd.read_sql(('Select Time as Time, GyroZ as Gyro, \'Z\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  # Query additional Flight data if desired
  if (rf2):
      qX2 = pd.read_sql(('Select Time as Time, GyroX as Gyro, \'X RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qY2 = pd.read_sql(('Select Time as Time, GyroY as Gyro, \'Y RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qZ2 = pd.read_sql(('Select Time as Time, GyroZ as Gyro, \'Z RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      return pd.concat([qX,qY,qZ,qX2,qY2,qZ2])
  return pd.concat([qX,qY,qZ])


##################################################################################################################
# This helper function creates a query for Accelerometer Data
##################################################################################################################
def query_acc():
  qX = pd.read_sql(('Select Time as Time, AccX as Acc, \'X\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  qY = pd.read_sql(('Select Time as Time, AccY as Acc, \'Y\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  qZ = pd.read_sql(('Select Time as Time, AccZ as Acc, \'Z\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  # Query additional Flight data if desired
  if (rf2):
      qX2 = pd.read_sql(('Select Time as Time, AccX as Acc, \'X RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qY2 = pd.read_sql(('Select Time as Time, AccY as Acc, \'Y RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qZ2 = pd.read_sql(('Select Time as Time, AccZ as Acc, \'Z RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      return pd.concat([qX,qY,qZ,qX2,qY2,qZ2])
  return pd.concat([qX,qY,qZ])


##################################################################################################################
# This helper function creates a query for Roll Pitch and Yaw
##################################################################################################################
def query_rollPitchYaw():
  qX = pd.read_sql(('Select Time as Time, Roll as rollPitchYaw, \'Roll\' as Axes FROM IMU_Data WHERE Flight_ID = ?'),conn,params=[rf1])
  qY = pd.read_sql(('Select Time as Time, Pitch as rollPitchYaw, \'Pitch\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  qZ = pd.read_sql(('Select Time as Time, Yaw as rollPitchYaw, \'Yaw\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf1])
  # Query additional Flight data if desired
  if (rf2):
      qX2 = pd.read_sql(('Select Time as Time, Roll as rollPitchYaw, \'Roll RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qY2 = pd.read_sql(('Select Time as Time, Pitch as rollPitchYaw, \'Pitch RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      qZ2 = pd.read_sql(('Select Time as Time, Yaw as rollPitchYaw, \'Yaw RF 2\' as Axes FROM IMU_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      return pd.concat([qX,qY,qZ,qX2,qY2,qZ2])
  return pd.concat([qX,qY,qZ])


##################################################################################################################
# This helper function creates a query for GPS Data
##################################################################################################################
def query_gps():
  # We use two queries here so that you can color the start differently than the rest of the flight.
  q1 = pd.read_sql((''' 
                SELECT Time as Time, PositionX as Latitude, PositionY as Longitude, PositionZ as Altitude, \'GPS\' as Start
                FROM GPS_Data
                WHERE Flight_ID = ? AND
                Time >= 1
                '''),conn,params=[rf1])
  q2 = pd.read_sql(('''
                SELECT Time as Time, PositionX as Latitude, PositionY as Longitude, PositionZ as Altitude, \'Start\' as Start
                FROM GPS_Data
                WHERE Flight_ID = ? AND
                Time <= 1
                '''),conn,params=[rf1])
  return pd.concat([q1,q2])


##################################################################################################################
# This helper function creates a query for Laser Data
##################################################################################################################
def query_Laser():
  q1 = pd.read_sql(('''
       SELECT Time as Time, Laser_Float as Distance,\'Original\' as Axes
       FROM Laser_Data
       Where Flight_ID = ? 
       '''),conn,params=[rf1])
  if(rf2):
    q2 = pd.read_sql(('''
       SELECT Time as Time, Laser_Float as Distance,\'RF 2\' as Axes
       FROM Laser_Data
       Where Flight_ID = ? 
       '''),conn,params=[rf2])
    return pd.concat([q1,q2])
  return q1


##################################################################################################################
# This helper function creates a query for Servo Data
##################################################################################################################
def query_Servo():
  q1 = pd.read_sql((''' SELECT Time as Time, Servo_One as ServoPos, \'Servo_One\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  q2 = pd.read_sql((''' SELECT Time as Time, Servo_Two as ServoPos, \'Servo_Two\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  q3 = pd.read_sql((''' SELECT Time as Time, Servo_Three as ServoPos, \'Servo_Three\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  q4 = pd.read_sql((''' SELECT Time as Time, Servo_Four as ServoPos, \'Servo_Four\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  q5 = pd.read_sql((''' SELECT Time as Time, Servo_Five as ServoPos, \'Servo_Five\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  q6 = pd.read_sql((''' SELECT Time as Time, Servo_Six as ServoPos, \'Servo_Six\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
  if (rf2):
        q7 = pd.read_sql((''' SELECT Time as Time, Servo_One as ServoPos, \'Servo_One 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        q8 = pd.read_sql((''' SELECT Time as Time, Servo_Two as ServoPos, \'Servo_Two 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        q9 = pd.read_sql((''' SELECT Time as Time, Servo_Three as ServoPos, \'Servo_Three 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        q10 = pd.read_sql((''' SELECT Time as Time, Servo_Four as ServoPos, \'Servo_Four 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        q11 = pd.read_sql((''' SELECT Time as Time, Servo_Five as ServoPos, \'Servo_Five 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        q12 = pd.read_sql((''' SELECT Time as Time, Servo_Six as ServoPos, \'Servo_Six 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        return pd.concat([q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12])
  return pd.concat([q1,q2,q3,q4,q5,q6])

##################################################################################################################
# This helper function creates a query for Fan Speed Data
##################################################################################################################
def query_Speed():
  # Servo Seven is the fan Throttle
    q = pd.read_sql((''' SELECT Time as Time, Servo_Seven as Throttle, \'Throttle\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf1])
    if (rf2):
        q2 = pd.read_sql((''' SELECT Time as Time, Servo_Seven as Throttle, \'Throttle 2\' as Axes FROM Servo Where Flight_ID = ?'''),conn,params=[rf2])
        return pd.concat([q,q2])
    return q


##################################################################################################################
# This helper function creates a query for Fan Speed Data
##################################################################################################################
def query_Gamma():
  # Servo Seven is the fan Throttle
    q = pd.read_sql((''' SELECT Time as Time, gamma, \'Gamma\' as Axes FROM IMU_Data Where Flight_ID = ?'''),conn,params=[rf1])
    if (rf2):
        q2 = pd.read_sql((''' SELECT Time as Time, gamma, \'Gamma 2\' as Axes FROM IMU_Data Where Flight_ID = ?'''),conn,params=[rf2])
        return pd.concat([q,q2])
    return q


##################################################################################################################
# This helper function creates a query for Gain Data
##################################################################################################################
def query_gain():
    q1 = pd.read_sql(('Select Time as Time, KpPitch as gain, \'P Pitch\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q2 = pd.read_sql(('Select Time as Time, KiPitch as gain, \'I Pitch\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q3 = pd.read_sql(('Select Time as Time, KdPitch as gain, \'D Pitch\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    pitchGains = pd.concat([q1,q2,q3])
    q4 = pd.read_sql(('Select Time as Time, KpRoll as gain, \'P Roll\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q5 = pd.read_sql(('Select Time as Time, KiRoll as gain, \'I Roll\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q6 = pd.read_sql(('Select Time as Time, KdRoll as gain, \'D Roll\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    rollGains = pd.concat([q4,q5,q6])
    q7 = pd.read_sql(('Select Time as Time, kpAlt as gain, \'P Yaw\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q8 = pd.read_sql(('Select Time as Time, kiAlt as gain, \'I Yaw\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    q9 = pd.read_sql(('Select Time as Time, kdAlt as gain, \'D Yaw\' as Axes FROM Gains WHERE Flight_ID = ? '),conn,params=[rf1])
    yawGains = pd.concat([q7,q8,q9])
    return pitchGains,rollGains,yawGains


##################################################################################################################
# This helper function creates a query for flight Metadata, shown in the DataTable on the Reporting Site
##################################################################################################################   
def query_Flights():
  # Currently not working
    q = pd.read_sql((''' Select Flight_ID, Date, Type, Location,
                         Description, PhaseZeroScore, PhaseOneScore
                         PhaseTwoScore, PhaseThreeScore, PhaseFourScore, 
                         PhaseFiveScore
                         from Rocket_flight where Flight_id > 350
                         ORDER BY Flight_ID DESC'''),conn)
    return q
   

##################################################################################################################
# This helper function returns the phase scores
##################################################################################################################
def query_phaseScores(rf):
  q = pd.read_sql(('''SELECT PhaseZeroScore, PhaseOneScore, PhaseTwoScore, PhaseThreeScore, PhaseFourScore, PhaseFiveScore
          FROM  Rocket_Flight
          WHERE Flight_ID = ? '''),conn,params=[rf])    
  if(len(q.values.tolist()) > 0 ):
    return(q.values.tolist()[0])
  else:
    # Help with error handling
    return [-1]

##################################################################################################################
# This helper function returns the laser scores
##################################################################################################################
def query_LaserScores(rf):
  q = pd.read_sql(('''SELECT Score
          FROM  Laser_Data
          WHERE Flight_ID = ? '''),conn,params=[rf])
  if(len(q.values.tolist()) > 0 ):
    return(q.values.tolist()[0])
  else:
    # Help with error handling
    return [-1]
##################################################################################################################
# This helper function returns the speed and acceleration calculated by the laser
##################################################################################################################
def query_LaserSpeedAcc():
    q1 = pd.read_sql(('''
          SELECT Time as Time, Speed as SpeedAcc,\'Speed\' as Axes
          FROM Laser_Data
          Where Flight_ID = ? 
          '''),conn,params=[rf1])
    q2 = pd.read_sql(('''
          SELECT Time as Time, Acceleration as SpeedAcc,\'Acceleration\' as Axes
          FROM Laser_Data
          Where Flight_ID = ? 
          '''),conn,params=[rf1])
    if (rf2):
          q3 = pd.read_sql(('''
              SELECT Time as Time, Speed as SpeedAcc,\'Speed 2\' as Axes
              FROM Laser_Data
              Where Flight_ID = ? 
              '''),conn,params=[rf2])
          q4 = pd.read_sql(('''
              SELECT Time as Time, Acceleration as SpeedAcc,\'Acceleration 2\' as Axes
              FROM Laser_Data
              Where Flight_ID = ? 
              '''),conn,params=[rf2])
          return pd.concat([q1,q2,q3,q4])
    
    return pd.concat([q1,q2])

##################################################################################################################
# This helper function returns Altitude vs Desired Altitude Vs Throttle
##################################################################################################################
def query_DesiredAlt():
    q1 = pd.read_sql(('''
          SELECT Time as Time, Laser_Float as Altitude,\'Altitude\' as Axes
          FROM Laser_Data
          Where Flight_ID = ? 
          '''),conn,params=[rf1])
    q2 = pd.read_sql(('''
          SELECT Time as Time, Desired_Altitude as Altitude,\'Desired Altitude\' as Axes
          FROM Laser_Data
          Where Flight_ID = ? 
          '''),conn,params=[rf1])
    q3 = pd.read_sql((''' 
          SELECT Time as Time, Servo_Seven as Altitude, \'Throttle\' as Axes FROM Servo Where Flight_ID = ?
          '''),conn,params=[rf1])
    if (rf2):
        q4 = pd.read_sql(('''
              SELECT Time as Time, Laser_Float as Altitude,\'Altitude 2\' as Axes
              FROM Laser_Data
              Where Flight_ID = ? 
              '''),conn,params=[rf2])
        q5 = pd.read_sql(('''
              SELECT Time as Time, Desired_Altitude as Altitude,\'Desired Altitude 2\' as Axes
              FROM Laser_Data
              Where Flight_ID = ? 
              '''),conn,params=[rf2])
        q6 = pd.read_sql((''' 
              SELECT Time as Time, Servo_Seven as Altitude, \'Throttle 2\' as Axes FROM Servo Where Flight_ID = ?
              '''),conn,params=[rf2])
        return pd.concat([q1,q2,q3,q4,q5,q6])      
    return pd.concat([q1,q2,q3])


##################################################################################################################
# This helper function returns the speed PID control
##################################################################################################################
def query_AltitudePID():
    q = pd.read_sql(('Select Time as Time, AltPID as pid, \'AltPID\' as Axes FROM Laser_Data WHERE Flight_ID = ? '),conn,params=[rf1])
    if (rf2):
      q2 = pd.read_sql(('Select Time as Time, AltPID as pid, \'AltPID 2\' as Axes FROM Laser_Data WHERE Flight_ID = ? '),conn,params=[rf2])
      return pd.concat([q,q2])
    return q


##################################################################################################################
# This helper function adds the phase coloring into each graph
##################################################################################################################
def add_phases(graph):
    graph.add_vrect(x0=0, x1=phases[0], annotation_text="PreTakeoff", 
                    annotation_position="top", line_width = 0, fillcolor='green',opacity=0.25)
    graph.add_vrect(x0=phases[0], x1=phases[1], annotation_text="Takeoff", 
                    annotation_position="top", line_width = 0, fillcolor='blue',opacity=0.25)
    graph.add_vrect(x0=phases[1], x1=phases[2], annotation_text="FlightPeak", 
                    annotation_position="top", line_width = 0, fillcolor='orange',opacity=0.25)
    graph.add_vrect(x0=phases[2], x1=phases[3], annotation_text="MajorDescent", 
                    annotation_position="top", line_width = 0, fillcolor='yellow',opacity=0.25)               
    graph.add_vrect(x0=phases[3], x1=phases[4], annotation_text="Landing", 
                    annotation_position="top", line_width = 0, fillcolor='red',opacity=0.25)
    graph.add_vrect(x0=phases[4], x1=phases[5], annotation_text="PostLanding", 
                    annotation_position="top", line_width = 0, fillcolor='lightseagreen',opacity=0.25)
    # Zoom the graph to where we begin takeoff
    graph.update_xaxes(range = [phases[0]-0.5,phases[4]+0.5])
    

##################################################################################################################
# This helper function adds the a colored label at the peak altitude
##################################################################################################################
def add_max_annotation(graph):
    graph.add_annotation(
          x=flightPeakTime[0],
          y=flightPeak,
          xref="x",
          yref="y",
          text=f"Peak={flightPeak}",
          showarrow=True,
          font=dict(
              family="Courier New, monospace",
              size=12,
              color="#ffffff"
              ),
          align="center",
          arrowhead=2,
          arrowsize=1,
          arrowwidth=2,
          arrowcolor="#636363",
          hovertext='Our rocket should be this color',
          ax=20,
          ay=30,
          bordercolor="#A6E327",
          borderwidth=5,
          borderpad=2,
          bgcolor="#FA11F2",
          opacity=0.7
          )


try:
  print(f'phases: {phases}')
  # Create all graphs
  df = query_gyro()
  gyroFig = px.line(df, x="Time", y="Gyro", color="Axes",title='Gyro Data').update_layout(autotypenumbers='convert types')
  df = query_acc()
  accFig = px.line(df, x="Time", y="Acc", color="Axes", title='Acceleration Data').update_layout(autotypenumbers='convert types')
  df = query_rollPitchYaw()
  rollPitchYawFig = px.line(df,x='Time',y='rollPitchYaw', color='Axes',title='Roll Pitch Yaw').update_layout(autotypenumbers='convert types',legend_bordercolor='red')
  df = query_gps()
  gpsFig = px.line_3d(df, x='Latitude',y='Longitude',z='Altitude',color='Start',title='3d GPS Track').update_layout(autotypenumbers='convert types')
  df = query_Laser()
  laserFig = px.line(df, x='Time',y='Distance',color='Axes',title='Laser Distance').update_layout(autotypenumbers='convert types')
  df = query_Servo()
  servoFig = px.line(df, x ='Time',y='ServoPos',color='Axes',title='Servo Positions').update_layout(autotypenumbers='convert types')
  df = query_Gamma()
  gammaFig = px.line(df,x ='Time', y='gamma',color='Axes', title='Gamma' ).update_layout(autotypenumbers='convert types')
  df = query_Speed()
  throttleFig = px.line(df,x='Time',y='Throttle',color='Axes',title='Fan Throttle').update_layout(autotypenumbers='convert types')
  df = query_LaserSpeedAcc()
  laserSpeedAccFig = px.line(df,x='Time',y='SpeedAcc',color='Axes',title='Laser Calculated Speed/Acceleration').update_layout(autotypenumbers='convert types')
  df = query_AltitudePID()
  altitudePIDFig = px.line(df,x ='Time', y='pid',color='Axes', title='Altitude PID' ).update_layout(autotypenumbers='convert types')
  df = query_DesiredAlt()
  desiredAltFig =  px.line(df,x='Time',y='Altitude',color='Axes',title='Desired Alt vs Alt').update_layout(autotypenumbers='convert types')
  dfPitch, dfRoll, dfYaw = query_gain()
  pitchGainFig = px.area(dfPitch,x ='Time', y='gain',color='Axes', title='Pitch Gains' ).update_layout(autotypenumbers='convert types')
  rollGainFig = px.area(dfRoll,x ='Time', y='gain',color='Axes', title='Roll Gains' ).update_layout(autotypenumbers='convert types')
  altGainFig = px.area(dfYaw,x ='Time', y='gain',color='Axes', title='Altitude Gains' ).update_layout(autotypenumbers='convert types')  
  df = query_Flights()
  # Add color coordinated phase mapping to each graph
  add_phases(gyroFig)
  add_phases(accFig)
  add_phases(rollPitchYawFig)
  add_phases(laserFig)
  add_phases(servoFig)
  add_phases(pitchGainFig)
  add_phases(rollGainFig)
  add_phases(altGainFig)
  add_phases(throttleFig)
  add_phases(gammaFig)
  add_phases(laserSpeedAccFig)
  add_phases(altitudePIDFig)
  add_phases(desiredAltFig)
  add_max_annotation(laserFig)
  add_max_annotation(desiredAltFig)


  
except sqlite3.Error as E:
    print(' * * * Error Inside createReport.py * * *')
    sys.exit(1)

# Grab scores for flights
phaseScore = query_phaseScores(rf1)
prevScore = query_phaseScores(int(rf1)-1)
# Grab Laser scores
laserScore = query_LaserScores(rf1)
prevLaserScore = query_LaserScores(int(rf1)-1)
prevLaserScore = prevLaserScore[0]
laserScore = laserScore[0]
# If its an invalid score just set it to 0
if (not laserScore):
  laserScore = 0

# Percent change from last flight 
phaseChange = []
# Was Change positive or negative
phaseIncOrDec = []
for x,y in zip(phaseScore,prevScore):
  # Check For errors
  if (x == None or y == None or x+y == 0):
    phaseChange.append('E')
    phaseIncOrDec.append(False)
  else:
    # Percent Difference between current and previous flight
    phaseChange.append(str(int(abs(x-y)/((x+y)/2) * 100))+'%')
    # Was the flight increasing or decreasing
    if (x < y):
      phaseIncOrDec.append(True)
    else:
      phaseIncOrDec.append(False)

#  Create Datapane report
report = dp.Report(
  dp.Page(
     blocks=[
            dp.HTML(header),
            dp.Group(
                dp.BigNumber(
                    heading="Pretakeoff", 
                    value=phaseScore[0],
                    change = phaseChange[0],
                    is_upward_change=phaseIncOrDec[0]
                ),
                dp.BigNumber(
                    heading="Takeoff", 
                    value=phaseScore[1],
                    change = phaseChange[1],
                    is_upward_change=phaseIncOrDec[1]
                ),
                dp.BigNumber(
                    heading="Flight Peak", 
                    value=phaseScore[2],
                    change = phaseChange[2],
                    is_upward_change=phaseIncOrDec[2]
                ),
                dp.BigNumber(
                    heading="Major Descent", 
                    value=phaseScore[3],
                    change = phaseChange[3],
                    is_upward_change=phaseIncOrDec[3]
                ),
                dp.BigNumber(
                    heading="Landing", 
                    value=phaseScore[4],
                    change = phaseChange[4],
                    is_upward_change=phaseIncOrDec[4]
                ),
                dp.BigNumber(
                    heading="Post landing", 
                    value=phaseScore[5],
                    change = phaseChange[5],
                    is_upward_change=phaseIncOrDec[5]
                ),
                columns=3
              ),
             rollPitchYawFig,laserFig,throttleFig],
     title="RPY, Dist, Throttle",
  ),
  dp.Page(
    blocks=[
            dp.HTML(header),
            dp.BigNumber(
                    heading="Laser Score", 
                    value= round(laserScore,2),
                    change = str(round((abs(laserScore-prevLaserScore)/((laserScore+prevLaserScore)/2)),2))+'%',
                    is_upward_change=laserScore < prevLaserScore,
            ),
            desiredAltFig,
            laserSpeedAccFig,
            altitudePIDFig,
        ],
    title="Luna Measurements",
  ),
  dp.Page(
     blocks = [dp.HTML(header),
              pitchGainFig,rollGainFig,altGainFig,servoFig],
     title="Gain, Servo"
  ),
  dp.Page(
      blocks=[dp.HTML(header),
              gyroFig,accFig],
      title="Gyro, Acc"
  ),
  dp.Page(
      blocks=[dp.HTML(header),
              rollPitchYawFig,pitchGainFig,rollGainFig,altGainFig,gyroFig,accFig,laserFig,servoFig,throttleFig,gpsFig,
              dp.Group(
                dp.BigNumber(
                    heading="Pretakeoff", 
                    value=phaseScore[0],
                    change = phaseChange[0],
                    is_upward_change=phaseIncOrDec[0]
                ),
                dp.BigNumber(
                    heading="Takeoff", 
                    value=phaseScore[1],
                    change = phaseChange[1],
                    is_upward_change=phaseIncOrDec[1]
                ),
                dp.BigNumber(
                    heading="Flight Peak", 
                    value=phaseScore[2],
                    change = phaseChange[2],
                    is_upward_change=phaseIncOrDec[2]
                ),
                dp.BigNumber(
                    heading="Major Descent", 
                    value=phaseScore[3],
                    change = phaseChange[3],
                    is_upward_change=phaseIncOrDec[3]
                ),
                dp.BigNumber(
                    heading="Landing", 
                    value=phaseScore[4],
                    change = phaseChange[4],
                    is_upward_change=phaseIncOrDec[4]
                ),
                dp.BigNumber(
                    heading="Post landing", 
                    value=phaseScore[5],
                    change = phaseChange[5],
                    is_upward_change=phaseIncOrDec[5]
                ),
                columns=3
              ),
              # dp.Attachment(file = f'../Data/CSV/Flight{rf1}.xlsx')
              ],
      title="All Data"
  ),  
  dp.Page(
      blocks= [
          dp.HTML(header),
          gammaFig,
          dp.Formula(r"acos(\frac{cos(r)}{\sqrt{1+(tan(p)^2)*cos(r)^2}})", caption="Formula for Gamma"),
      ],
      title="Gamma"
  ),
  dp.Page(
    blocks=[
      dp.DataTable(df, caption='All Flights'),
    ],
    title='Flight Metadata'
  ),
)

report.save(path=saveFile, open=True)
# report.upload(name=f'Datapane Flight data {rf1}',open=True,
#     formatting=dp.ReportFormatting(
#             light_prose=False, 
#             accent_color="orange", 
#             bg_color="#FFF", 
#             text_alignment=dp.TextAlignment.RIGHT,
#             font=dp.FontChoice.MONOSPACE,
#             width=dp.ReportWidth.FULL
#         ))

print(f'Time to create Report: {round(time.time()-t1 ,3)}')

